function [pm] = Biseccion(f,a,b,tol)
  j=eps();
  printf("Epsilon de la màquina: ");
  disp(j);
  tramo=b-a;
  %Calcula el maximo de iteraciones
  maxIter = ceil((log(b-a)-log(tol))/log(2));
  if f(a)*f(b)>0
    printf("Intervalo donde no hay cambio de signo...\n");
    return
  endif
  X_anterior=0;
  i=0;
  %Iteracion, valor de a,b,c
  I=[]; Xa=[]; Xb=[]; Xc=[];
  %Valor de f(a), f(b) y f(c)
  Fa=[]; Fb=[]; Fc=[]; 
  %Obtener el signo del producto de f(a)*f(c)
  Fac=[];
  %Error y matriz para guardar en archivo
  E=[]; z=[];
  
  while (tramo>tol)
    c=(a+b)/2;
      X_actual=c;
      error = abs(((X_actual-X_anterior)/X_actual)*100);
      i+=1;
      if i==1
        error =0;
      endif
      I=[I;i];
      Xa=[Xa;a];
      Xb=[Xb;b];
      Xc=[Xc;c];
      Fa=[Fa;f(a)];
      Fb=[Fb;f(b)];
      Fc=[Fc;f(c)];
      Fac=[Fac;f(a)*f(c)];
      E=[E;error];
      if f(a)*f(c)<0
        b=c;
       else
        a=c;
      endif
      tramo=b-a;
      c=(a+b)/2;
      X_anterior=X_actual;
      r=X_anterior;
    endwhile
   %guarda las matrices columna dentro de la matriz z
   z=[z;I,Xa,Xb,Xc,Fa,Fb,Fc,Fac,E];
   %Guarda la tabla de resultados en un archivo csv sin texto de cabecera
   save tabla_biseccion.csv -ascii z
   %Imprime la tabla en pantalla (Implementaciòn en Octave).
   tab = table(I,Xa,Xb,Xc,Fa,Fb,Fc,Fac,E);
   prettyprint(tab);
   printf("Archivo generado: tabla_biseccion.csv \n")
   printf("Aproximaciòn de raiz: r = %2.10f\n",r);
   printf("Iteraciones: i = %i\n",i);
   printf("Error: %1.16f\n",tramo);  
endfunction